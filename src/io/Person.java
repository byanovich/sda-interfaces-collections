package io;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;

public class Person {

    private String name;

    private int age;

    private List<String> phoneNumbers = new ArrayList<>();

    public Person(String name, int age, String... phoneNumbers) {
        this.name = name;
        this.age = age;

        for (String phoneNumber : phoneNumbers) {
            this.phoneNumbers.add(phoneNumber);
        }

    }

    public List<String> getPhoneNumbers() {
        return Collections.unmodifiableList(phoneNumbers);
    }

    public int getAge() {
        return age;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", phoneNumbers=" + phoneNumbers +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;

//        name.equals(person.name);
//        phoneNumbers.equals(person.phoneNumbers);

        return age == person.age &&
                Objects.equals(name, person.name) &&
                Objects.equals(phoneNumbers, person.phoneNumbers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age, phoneNumbers);
    }
}
