package ee.sda.interfaces;

public class GenericBoxForTwoItems <T, S> extends GenericBox<T> {

    private S item2;

    public S getItem2() {
        return item2;
    }

    public void setItem2(S item2) {
        this.item2 = item2;
    }
}
