package ee.sda.interfaces;

public class WrapperBox implements Shape {

    private static final double boxSize = 2.5;

    @Override
    public double getWidth() {
        return boxSize;
    }

    @Override
    public double getLength() {
        return boxSize;
    }

    @Override
    public double getArea() {
        return boxSize * boxSize;
    }
}
