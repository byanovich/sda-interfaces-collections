package ee.sda.interfaces;

public interface Swimmer {

    int DEFAULT_SPEED = 20;

    void swim();

    default double getSwimmingSpeed() {
        return DEFAULT_SPEED;
    }

}
