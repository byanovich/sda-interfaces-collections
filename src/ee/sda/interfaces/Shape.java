package ee.sda.interfaces;

public interface Shape {

    double getWidth();

    double getLength();

    double getArea();

}
