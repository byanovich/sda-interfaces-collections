package ee.sda.interfaces;

import java.util.*;

public class InterfacesMain {

    public static void main(String[] args) {
        Shape[] shapes = new Shape[10];

        for (int i = 0; i < shapes.length; i++) {
            Shape shape;
            if (i % 2 == 0) {
                shape = new WrapperBox();
            } else {
                Random random = new Random();
                shape = new Book(random.nextInt(10) + 1,
                        random.nextInt(20) + 1);
            }

            shapes[i] = shape;
        }

        for (Shape shape : shapes) {
            System.out.println("Shape: type: " + shape.getClass() +
                    " width: " + shape.getWidth() +
                    " length: " + shape.getLength() +
                    " area: " + shape.getArea());
        }

        double totalWidth = 0;
        double totalLength = 0;
        double totalUsedArea = 0;
        for (Shape shape : shapes) {
            totalLength += shape.getLength();
            totalWidth = Math.max(totalWidth, shape.getWidth());
            totalUsedArea += shape.getArea();
        }

        double totalAreaOfContainer = totalLength * totalWidth;
        double effectivelyUsedRatio = totalUsedArea / totalAreaOfContainer;

        System.out.println("Total width: " + totalWidth +
                " total length: " + totalLength +
                " total used area: " + totalUsedArea +
                " total area of the container: " + totalAreaOfContainer +
                " effectiveness:  " + effectivelyUsedRatio);

        Person person = new Person();
        person.swim();
        person.meow();

        Cat cat = new Cat();
        cat.meow();

        Box boxWithBook = new Box();
        boxWithBook.setItem(new Book(14, 20));

        Book book = (Book) boxWithBook.getItem();
        System.out.println("Area of the book " + book.getArea());

        Box bowWithBook2 = new Box();
        bowWithBook2.setItem(new Book(4, 2));
        Book bookFromTheBox = (Book) bowWithBook2.getItem();
        System.out.println("Area of the second book " +
                bookFromTheBox.getArea());

        Box boxWithCat = new Box();
        boxWithCat.setItem(cat);
        Cat catFromTheBox = (Cat) boxWithCat.getItem();
        catFromTheBox.meow();

        Box[] cargo = new Box[3];
        cargo[0] = boxWithBook;
        cargo[1] = bowWithBook2;
        cargo[2] = boxWithCat;

        for (Box currentBox : cargo) {
            Object itemInTheBox = currentBox.getItem();

            if (itemInTheBox instanceof Book) {
                Book b = (Book) itemInTheBox;
                System.out.println("Book area: " + b.getArea());
            } else if (itemInTheBox instanceof Cat) {
                Cat c = (Cat) itemInTheBox;
                c.meow();
            }
        }

        GenericBox<Book> genericBoxForBook = new GenericBox<>();
        genericBoxForBook.setItem(new Book(3, 7));
//        genericBoxForBook.setItem(new Cat());
        bookFromTheBox = genericBoxForBook.getItem();
        System.out.println("Area of the book " +
                bookFromTheBox.getArea());

        GenericBox<Cat> genericBoxForCat = new GenericBox<>();
        genericBoxForCat.setItem(new Cat());
        catFromTheBox = genericBoxForCat.getItem();
        catFromTheBox.meow();


        GenericBoxForTwoItems<Cat, Book> bigBox =
                new GenericBoxForTwoItems<>();
        bigBox.setItem(cat);
        bigBox.setItem2(book);

        catFromTheBox = bigBox.getItem();
        bookFromTheBox = bigBox.getItem2();

        System.out.println("---");

        System.out.println("Book area: " +
                bookFromTheBox.getArea());
        catFromTheBox.meow();

        // you can put anything
        GenericBox<?> boxWithAnything =
                new GenericBox<>();

        // upper boundary
        GenericBox<? extends Book> boxWithAnyTypeOfBook =
                new GenericBox<>();

        // lower boundary
        GenericBox<? super Book> boxWithParentsOfBook =
                new GenericBox<>();


        ArrayList<String> shoppingList =
                new ArrayList<>();

        shoppingList.add("Milk");
        shoppingList.add("Bread");
        shoppingList.add("Cheese");
        shoppingList.add("Ham");

        System.out.println("Shopping list: " +
                "totalItems: " + shoppingList.size());

        System.out.println(shoppingList.get(3));

        Iterator<String> shoppingListIterator =
                shoppingList.iterator();

        while (shoppingListIterator.hasNext()) {
            String shoppingItem =
                    shoppingListIterator.next();

            System.out.println("Item: " + shoppingItem);
        }

        for (String shoppingItem : shoppingList) {
            System.out.println("For-each loop item: " +
                    shoppingItem);
        }

        System.out.println("Removing items: ");

        shoppingList.remove(3);
        shoppingList.remove("Milk");

        for (String shoppingItem : shoppingList) {
            System.out.println("Left to buy: " +
                    shoppingItem);
        }

        System.out.println("Need to buy water? " +
                shoppingList.contains("Water"));

        System.out.println("Need to buy Bread? " +
                (shoppingList.indexOf("Bread") >= 0));

        System.out.println("Need to buy water? " +
                (shoppingList.indexOf("Water") >= 0));


        LinkedList<String> testLinkedList =
                new LinkedList<>();


        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            testLinkedList.add(0, "Some string");
        }
        long finishTime = System.currentTimeMillis();

        System.out.println("LinkedList add totalTime " +
                (finishTime - startTime));

        ArrayList<String> testArrayList =
                new ArrayList<>();


        startTime = System.currentTimeMillis();
        for (int i = 0; i < 100000; i++) {
            testArrayList.add(0, "Some string");
        }
        finishTime = System.currentTimeMillis();

        System.out.println("ArrayList add totalTime " +
                (finishTime - startTime));

        startTime = System.currentTimeMillis();
        for (int i = 0; i < testLinkedList.size(); i++) {
            String s = testLinkedList.get(0);
            s += "";
        }
        finishTime = System.currentTimeMillis();

        System.out.println("LinkedList get totalTime " +
                (finishTime - startTime));

        startTime = System.currentTimeMillis();
        for (int i = 0; i < testArrayList.size(); i++) {
            String s = testArrayList.get(0);
            s += "";
        }
        finishTime = System.currentTimeMillis();

        System.out.println("ArrayList get totalTime " +
                (finishTime - startTime));

        startTime = System.currentTimeMillis();
        for (int i = testLinkedList.size() - 1; i >= 0; i--) {
            testLinkedList.remove(i);
        }
        finishTime = System.currentTimeMillis();

        System.out.println("LinkedList remove totalTime " +
                (finishTime - startTime));

        startTime = System.currentTimeMillis();
        for (int i = testArrayList.size() - 1; i >= 0; i--) {
            testArrayList.remove(i);
        }
        finishTime = System.currentTimeMillis();

        System.out.println("ArrayList remove totalTime " +
                (finishTime - startTime));


        LinkedList<String> waitingList =
                new LinkedList<>();

        for (int i = 0; i < 5; i++) {
            waitingList.addLast("Person " + i);
        }

        while (!waitingList.isEmpty()) {
            String personToService = waitingList.peek();
            System.out.println(personToService + " serviced");
            waitingList.remove();
        }

        PriorityQueue<Person> personPriorityQueue =
                new PriorityQueue<>(new Comparator<Person>() {
                    @Override
                    public int compare(Person o1, Person o2) {

                        boolean firstPensioner = o1.getAge() > 65;
                        boolean secondPensioner = o2.getAge() > 65;

                        if (firstPensioner && !secondPensioner) {
                            return -1;
                        }

                        if (!firstPensioner && secondPensioner) {
                            return 1;
                        }

                        return 0;
                    }
                });

        personPriorityQueue.add(new Person(30));
        personPriorityQueue.add(new Person(70));
        personPriorityQueue.add(new Person(15));
        personPriorityQueue.add(new Person(73));
        personPriorityQueue.add(new Person(20));

        while (!personPriorityQueue.isEmpty()) {
            Person servicedPerson = personPriorityQueue.poll();
            System.out.println("Serviced Person of age "
                    + servicedPerson.getAge());
        }

        HashSet<String> visitedCities = new HashSet<>();
        visitedCities.add("Tartu");
        visitedCities.add("Tallinn");
        visitedCities.add("Rapla");
        visitedCities.add("Tallinn");
        visitedCities.add("Narva");
        visitedCities.add("Narva");

        for (String city : visitedCities) {
            System.out.println("I visited " + city + " one day");
        }

        visitedCities.remove("Narva");
        visitedCities.remove("Rapla");

        System.out.println("After removing: ");
        for (String city : visitedCities) {
            System.out.println("I visited " + city + " one day");
        }

        Book book1 = new Book(12, 15);
        Book book2 = new Book(25, 33);

        System.out.println("Equals? " + book1.equals(book2));

        Book book3 = new Book(12, 15);

        System.out.println("Equals 1 & 3? " + book1.equals(book3));

        Set<Book> readBooks = new HashSet<>();
        readBooks.add(book1);
        readBooks.add(book2);
        readBooks.add(book3);

        for (Book readBook : readBooks) {
            System.out.println("Read book: " + readBook);
        }


        HashMap<Book, String> booksAuthorsMap = new HashMap<>();

        booksAuthorsMap.put(new Book(1, 2), "Author A");
        booksAuthorsMap.put(new Book(3, 4), "Author B");
        booksAuthorsMap.put(new Book(4, 3), "Author A");
        booksAuthorsMap.put(new Book(89, 22), "Author C");

        for (Book bookFromMap : booksAuthorsMap.keySet()) {
            System.out.println("Book from map: " + bookFromMap);
        }

        for (String authorFromMap : booksAuthorsMap.values()) {
            System.out.println("Author from map: " + authorFromMap);
        }

        for (Map.Entry<Book, String> bookAuthor : booksAuthorsMap.entrySet()) {
            System.out.println("Mapping: book " + bookAuthor.getKey() +
                    " -> author " + bookAuthor.getValue());
        }

        System.out.println("Author of 1x2 is " +
                booksAuthorsMap.get(new Book(1, 2)));

        System.out.println("Author of 10x20 is " +
                booksAuthorsMap.get(new Book(10, 20)));

        System.out.println("Author of 10x20 is " +
                booksAuthorsMap.getOrDefault(new Book(10, 20),
                        "Unknown author"));

        // TODO TreeSet
        // TODO TreeMap



    }

}
