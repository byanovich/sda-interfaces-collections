package ee.sda.interfaces;

import java.util.Objects;

public class Book implements Shape, Comparable<Book> {

    private int width;

    private int length;

    private String name;

    public Book(int width, int length) {
        this.width = width;
        this.length = length;
    }

    @Override
    public int compareTo(Book otherBook) {
        return name.compareTo(otherBook.name);
//        return (int) (getArea() - otherBook.getArea());
    }

    @Override
    public double getWidth() {
        return width;
    }

    @Override
    public double getLength() {
        return length;
    }

    @Override
    public double getArea() {
        return width * length;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Book) {
            Book book = (Book) obj;
            return width == book.width && length == book.length;
        }
        return false;
    }

    @Override
    public int hashCode() {
//        return width * length;
//        // 10 * 10
//        // 2 * 50
        return Objects.hash(width, length);
    }

    @Override
    public String toString() {
        return "Book{" +
                "width=" + width +
                ", length=" + length +
                '}';
    }
}
