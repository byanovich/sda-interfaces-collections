package io;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class IOMain {

    public static void main(String[] args) throws FileNotFoundException,
            IOException, ClassNotFoundException {
        Person person = new Person("Teet Tee", 28,
                "tel123", "tel467", "tel9690");
        System.out.println(person);

        int personAge = person.getAge();
        System.out.println("age: " + personAge);

        personAge = 50;
        System.out.println("modified age var: " + personAge);

        System.out.println("person age is still " + person.getAge());


        List<String> personNumbers = new ArrayList<>(person.getPhoneNumbers());
        System.out.println("nums: " + personNumbers);

        personNumbers.add("tel NEW NUMBER");

        System.out.println("modified numbers var: " + personNumbers);

        System.out.println(person);


        // Helpers
        //Collections.
        //Arrays.
        //Objects.

//        Objects.deepEquals();
//        Objects.equals();


        FileInputStream inputStream = null;
        FileOutputStream outputStream = null;

        try {
            inputStream = new FileInputStream("cat.jpg");
            outputStream = new FileOutputStream("catCopy.jpg");

            int c = inputStream.read();

            while (c != -1) {
                outputStream.write(c);
                c = inputStream.read();
            }
        } finally {
            if (inputStream != null) {
                inputStream.close();
            }
            if (outputStream != null) {
                outputStream.close();
            }
        }

        FileReader inputReader = null;
        FileWriter outputWriter = null;

        try {
            inputReader = new FileReader("input.txt");
            outputWriter = new FileWriter("output.txt");

            int c = inputReader.read();

            while (c != -1) {
                outputWriter.write(c);
                System.out.println( (char) c);

                c = inputReader.read();
            }

        } finally {
            if (inputReader != null) {
                inputReader.close();
            }
            if (outputWriter != null) {
                outputWriter.close();
            }
        }

        System.out.println("Buffered: ");



        List<String> linesFromFile = new ArrayList<>();

        try (
                BufferedReader bufferedReader = new BufferedReader(
                new FileReader("input.txt"))
        ) {

            String line = bufferedReader.readLine();

            while (line != null) {
                linesFromFile.add(line);
                System.out.println(line);
                line = bufferedReader.readLine();
            }
        }

        try (BufferedWriter bufferedWriter = new BufferedWriter(
                new FileWriter("output2.txt")
        )) {
            for (String s : linesFromFile) {
                bufferedWriter.write(s);
                bufferedWriter.flush();
            }
        }


        Scanner scanner = new Scanner(
                new BufferedReader(
                        new FileReader("input2.txt")
                )
        );

        while (scanner.hasNextInt()) {
            int number = scanner.nextInt();
            System.out.println("Number from file: " + number);
        }

//        int n = scanner.nextInt();
//        scanner.nextLine();
//        String s = scanner.nextLine();
//
//        double d = scanner.nextDouble();
//
//        String fullLine = scanner.nextLine();
//        int intFromLine = Integer.parseInt(fullLine);
//
//        try {
//            double doubleFromLine = Double.parseDouble(fullLine);
//        } catch (NumberFormatException e) {
//            //
//        }

        // 11.4
        // 11,4
        // 2019-02-03
        // 03-02-2019

        BusinessCard businessCard1 = new BusinessCard(
                "Teet Tee", "+372-123123123",
                "teet@tee.ee"
        );

        try(BufferedWriter bufferedWriter = new BufferedWriter(
                new FileWriter("businessCard.txt")
        )) {
            bufferedWriter.write(businessCard1.getName());
            bufferedWriter.newLine();
            bufferedWriter.write(businessCard1.getPhoneNumber());
            bufferedWriter.newLine();
            bufferedWriter.write(businessCard1.getEmail());
            bufferedWriter.newLine();
        }

        BusinessCard businessCard2 = null;
        try(BufferedReader bufferedReader = new BufferedReader(
                new FileReader("businessCard.txt")
        )) {
            String name = bufferedReader.readLine();
            String phoneNumber = bufferedReader.readLine();
            String email = bufferedReader.readLine();
            businessCard2 =
                    new BusinessCard(name, phoneNumber, email);
        }

        System.out.println(businessCard2);


        List<BusinessCard> businessCards = new ArrayList<>();

        BusinessCard businessCardTemp = null;
        try(BufferedReader bufferedReader = new BufferedReader(
                new FileReader("businessCards.txt")
        )) {

            while (true) {
                String name = bufferedReader.readLine();
                String phoneNumber = bufferedReader.readLine();
                String email = bufferedReader.readLine();
                if (name == null || phoneNumber == null
                        || email == null) {
                    break;
                }
                businessCardTemp =
                        new BusinessCard(name, phoneNumber, email);
                businessCards.add(businessCardTemp);
            }

        }

        System.out.println(businessCards);

        ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                new FileOutputStream("businessCardsSerialized.txt")
        );

        for (BusinessCard card : businessCards) {
            objectOutputStream.writeObject(card);
        }

        objectOutputStream.writeObject(businessCards);

        objectOutputStream.close();

        ObjectInputStream objectInputStream = new ObjectInputStream(
                new FileInputStream("businessCardSerialized.txt")
        );

        BusinessCard bc = (BusinessCard) objectInputStream.readObject();

        System.out.println(bc);

        objectInputStream.close();



        // java.nio.*

        Path path = Paths.get("input.txt");
        Path absolutePath = Paths.get("C:\\Users");
        absolutePath =
                Paths.get("/tmp/anotherFolder/yetAnotherFolder");

        path.getParent();
        path.getRoot();

        Path nonsenseFile = Paths.get("nonsense.txt");
        List<String> allLines = Files.readAllLines(nonsenseFile);

        int totalWords = 0;
        Map<String, Integer> wordCounter = new HashMap<>();

        for (String line : allLines) {
            System.out.println(line);
            String[] words = line.split("\\s");
            System.out.println("words in a line: " + words.length);
            totalWords += (words.length - 1);

            for (String word : words) {

                word = word.toLowerCase().replaceAll("\\W", "");

                int currentNumberOfWords =
                        wordCounter.getOrDefault(word, 0);
                wordCounter.put(word, currentNumberOfWords + 1);
            }
        }


        System.out.println("Total words: " + totalWords);

        for (Map.Entry<String, Integer> wordCountPair :
                wordCounter.entrySet()) {
            System.out.println("Word: " + wordCountPair.getKey() +
                    " - " + wordCountPair.getValue() + " times");
        }

    }


}
