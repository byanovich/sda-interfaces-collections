package concurrency;

public class StringPrinter extends Thread {

    @Override
    public void run() {
        for (int i = 0; i < 10; i++) {

            System.out.println(
                    Thread.currentThread().getName() +
                    ": String" + i);
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
                // e.printStackTrace();
            }

        }
    }
}
