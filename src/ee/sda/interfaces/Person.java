package ee.sda.interfaces;

public class Person implements Swimmer, Meowable {

    private int age;

    public Person(int age) {
        this.age = age;
    }

    public Person() {

    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }

    @Override
    public void meow() {
        System.out.println("I'm meowing");
    }

    @Override
    public void swim() {
        System.out.println("I'm swimming");
    }

    @Override
    public double getSwimmingSpeed() {
        return 40;
    }
}
